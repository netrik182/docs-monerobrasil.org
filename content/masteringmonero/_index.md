---
title: "Mastering Monero"
date: 2020-07-17T17:39:38-03:00
draft: false
---

"Mastering Monero: The future of private transactions" (o futuro das transações privadas, em tradução livre) é o seu guia através do mundo do Monero, uma criptomoeda com foco em transações privadas e resistentes à censura. Este livro contém tudo o que você precisa saber para começar a usar Monero em seus negócios ou no dia-a-dia, mesmo que você ainda não tenha entendido ou interagido com criptomoedas anteriormente.

Todos os créditos pelo livro são do autor [SerHack](https://masteringmonero.com/)
